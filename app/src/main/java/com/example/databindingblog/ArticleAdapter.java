package com.example.databindingblog;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.databindingblog.databinding.ArticleItemBinding;

import java.util.List;

/**
 * Created by milan on 17.8.16.
 */
public class ArticleAdapter extends RecyclerView.Adapter<ArticleAdapter.BindingHolder> {

    private List<Article> mArticles;
    private Context mContext;

    public ArticleAdapter(List<Article> mArticles, Context mContext) {
        this.mArticles = mArticles;
        this.mContext = mContext;
    }

    @Override
    public BindingHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        /*
        * DataBinding creates XXBinding automatically
        * the XX comes from the name of related xml layout which has
        * <data> tag in it
        * */
        ArticleItemBinding binding = DataBindingUtil.inflate(
                LayoutInflater.from(parent.getContext()),
                R.layout.article_item, parent, false);

        return new BindingHolder(binding);
    }

    @Override
    public void onBindViewHolder(BindingHolder holder, int position) {
        ArticleItemBinding binding = holder.binding;
        /*
        * Avm is the name of <data> tag inside the xml layout that
        * you can name it anything you want
        * this method also is made automatically and you should pass
        * your ViewModel class to it
        * */
        binding.setAvm(new ArticleViewModel(mArticles.get(position), mContext));
    }

    @Override
    public int getItemCount() {
        return mArticles.size();
    }

    public static class BindingHolder extends RecyclerView.ViewHolder {
        private ArticleItemBinding binding;

        public BindingHolder(ArticleItemBinding binding) {
            /*
            * concatCard is the id of CardView inside the binding layout
            * you can access any item of the layout
            * */
            super(binding.contactCard);
            this.binding = binding;
        }
    }
}
