package com.example.databindingblog;

import com.example.databindingblog.BR;

import android.content.Context;
import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.databinding.BindingAdapter;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

/**
 * Created by milan on 21.8.16.
 */
public class ArticleViewModel extends BaseObservable {

    private Article mArticle;
    private Context mContext;

    public ArticleViewModel(Article mArticle, Context mContext) {
        this.mArticle = mArticle;
        this.mContext = mContext;
    }


    /*
    * In this class you can define your custom logic of the view
    * for ex you can define your custom getter and setter here
    *
    * */


    /*
    * @Bindable is used for getter of a variable which
    * has to act in observable way
    * it means that if the item changed anytime, the view
    * hast to refresh to
    * */
    @Bindable
    public String getTitle() {
        return mArticle.getTitle();
    }

    public void setTitle(String title) {
        mArticle.setTitle(title);
        notifyPropertyChanged(BR._all);
    }


    public int getCardBackgroundColor() {
        return mArticle.isHighlight() ?
                ContextCompat.getColor(mContext, R.color.highlight) :
                Color.parseColor("#ffffffff");
    }

    public int getCommentsButtonVisibility() {
        return mArticle.getCommentsNumber() == 0 ?
                View.GONE : View.VISIBLE;
    }

    public int getCommentsNumber() {
        return mArticle.getCommentsNumber();
    }

    public String getExcerpt() {
        return mArticle.getExcerpt();
    }


    /*
    *you can define custom attributes inside your xml layout
    * for ex i have defined "app:setare" then i set imageUrl to it for my Imageview
    * it means that i want to have customize behavior for loading my image
    * so after defining a generam getter for ImageUrl
    * i can define a customize Adapter with @BindingAdapter for this
    * and for ex i can load my image with Glide instead
    * */
    public String getImageUrl() {
        return mArticle.getImageUrl();
    }

    @BindingAdapter({"setare"})
    public static void setImageUrl(ImageView view, String url) {
        Glide.with(view.getContext()).load(url).centerCrop().into(view);
    }


    /*
    * Onclicks:
    * */

    public View.OnClickListener onReadMoreClicked() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(), "Opens article detail", Toast.LENGTH_SHORT).show();
                setTitle("Its Read");
            }
        };
    }

    public View.OnClickListener onCommentsClicked() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(view.getContext(), "Opens comments detail", Toast.LENGTH_SHORT).show();
            }
        };
    }
}
